import math
import numpy as np
import scipy
import mdp

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA


class PCArissom:

	def __init__(self):
		'''
			Construtor
		'''
		self.pca = PCA()

	def fit(self, data) :
		'''
			Fit
		'''
		# self.pca.fit(data)

		# transData = self.pca.transform(data)

		# self.iv = np.linalg.inv(np.cov(transData, rowvar=0))

		newPCArissom = PCArissom()
		newPCArissom.pca.fit(data)

		transData = newPCArissom.pca.transform(data)
		newPCArissom.iv = np.linalg.inv(np.cov(transData, rowvar=0))
		newPCArissom.std = np.std(transData, axis=0)
		newPCArissom.transData = transData

		return newPCArissom

		

	def get_error(self, data, model) :
		'''
		   Get Error
		'''
		
		# transData = np.dot(model.pca.components_,data.T)
		# transData = np.dot(model.pca.components_.T, transData)
		transData = model.pca.transform(data)

		distances = list()
		for inst in transData:
			distances.append(scipy.spatial.distance.mahalanobis(inst, model.pca.mean_, model.iv))

		return np.array(distances)

	