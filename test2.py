# from pylab import *

# t = arange(0.0, 2.0, 0.01)
# s = sin(2*pi*t)
# plot(t, s)

# xlabel('time (s)')
# ylabel('voltage (mV)')
# title('About as simple as it gets, folks')
# grid(True)
# savefig("test.png")
# show()

# import numpy as np
# import matplotlib.pyplot as plt

# def f(t):
#     return np.exp(-t) * np.cos(2*np.pi*t)

# t1 = np.arange(0.0, 5.0, 0.1)
# t2 = np.arange(0.0, 5.0, 0.02)

# plt.figure(1)
# plt.subplot(211)
# plt.plot(t1, f(t1), 'bo', t2, f(t2), 'k')

# plt.subplot(212)
# plt.plot(t2, np.cos(2*np.pi*t2), 'r--')
# plt.show()
import numpy as np
import matplotlib.pyplot as plt
from ransac import *
import math
from pca import PCArissom

from sklearn.decomposition import PCA


def test():
	
	# np.random.seed(5)
	a = np.random.normal(loc=0., scale=2.4, size=(60,2))
	b = np.random.normal(loc=0, scale=0.3, size=(10,2))

	for point in a:
		point[0] *= 20
		point[1] *= 0.1

	for point in b:
		point[1] += 3.5

	inst = np.concatenate((a,b), axis=0)
	# inst = a

	plt.plot(inst[:,0], inst[:,1], "ro")
	
	pca = PCA()
	pca.fit(inst)
	print pca.components_
	print pca.mean_

	pca = PCArissom()

	pca.fit(inst)
	# print pca.get_error(inst[0:10], pca)
	# print pca.get_error(inst[10:20], pca)
	# print pca.get_error(inst[20:30], pca)
	# print pca.get_error(inst[30:40], pca)
	# print pca.get_error(inst[40:50], pca)
	# print pca.get_error(inst[50:60], pca)
	# print pca.get_error(inst[60:70], pca)

	pca = ransac(inst, pca, 7, 500, 1.0,35)
	print pca.pca.components_
	print pca.pca.mean_
	print pca.std**2

	plt.plot(pca.transData[:,0], pca.transData[:,1], "bo")

	plt.grid(True)
	plt.show()

def main() :
	test()

if __name__ == "__main__":
    main()