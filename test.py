# from pylab import *

# t = arange(0.0, 2.0, 0.01)
# s = sin(2*pi*t)
# plot(t, s)

# xlabel('time (s)')
# ylabel('voltage (mV)')
# title('About as simple as it gets, folks')
# grid(True)
# savefig("test.png")
# show()

# import numpy as np
# import matplotlib.pyplot as plt

# def f(t):
#     return np.exp(-t) * np.cos(2*np.pi*t)

# t1 = np.arange(0.0, 5.0, 0.1)
# t2 = np.arange(0.0, 5.0, 0.02)

# plt.figure(1)
# plt.subplot(211)
# plt.plot(t1, f(t1), 'bo', t2, f(t2), 'k')

# plt.subplot(212)
# plt.plot(t2, np.cos(2*np.pi*t2), 'r--')
# plt.show()
import numpy as np
import matplotlib.pyplot as plt

import math

def dotproduct(v1, v2):
	return sum((a*b) for a, b in zip(v1, v2))

def length(v):
	return math.sqrt(dotproduct(v, v))

def angle(v1, v2, d=2):
	dp = dotproduct(v1[:d], v2[:d]) / (length(v1[:d]) * length(v2[:d]))
	dp = min(1.0, max(-1.0, dp))
	return math.acos(dp)

def cleanData(dataX, dataY) :
	numInstances = dataX.shape[0]

	cData = np.zeros(shape=(numInstances, 2))
	cData[:,0] = np.copy(dataX)
	cData[:,1] = np.copy(dataY)
	# # print cData
	# # exit()
	# for instance in cData :
	# 	# print "instance: ", instance
	# 	for neighbor in cData:
	# 		if not (instance == neighbor).all():
	# 			for neighbor2 in cData:
	# 				if not (instance == neighbor2).all() and not (neighbor == neighbor2).all():
	# 					instance[2] += max(angle((neighbor - instance),(neighbor2 - instance)), angle((neighbor - instance),-(neighbor2 - instance)))
	# 					# print  neighbor, neighbor2 ,max(angle((neighbor - instance),(neighbor2 - instance)), angle((neighbor - instance),-(neighbor2 - instance)))
	# 				# print instance
	# 	# exit()
	# 	# print ""
	# print cData
	# exit()
	anglesSum = list()
	for i in range(numInstances) :
		angleS = 0.0
		
		vectors = list()
		for j in range(numInstances) :
			if i != j :
				vectors.append(cData[j] - cData[i])

		size = len(vectors)
		for k in range(size) :
			for l in range(size):
				if k != l :
					angleS += angle(vectors[k], vectors[l])

		anglesSum.append((cData[i], angleS))

	####
	return sorted(anglesSum, key=lambda anglesSum: anglesSum[1])


def test():
	

	np.random.seed(5)
	x = np.arange(1, 41)
	y = 20 + 3 * x + np.random.normal(0, 60, 40)
	x = np.insert(x, 40, 40)
	y = np.insert(y, 0, 40)

	# x = np.array([0,1,2,3,4,5,6,7])
	# y = np.array([0,1,2,3,4,5,6,3])

	c = cleanData(x, y)
	# print c
	# exit()
	for inst, ang in c[:5]:
		plt.plot(inst[0], inst[1], "bo")
		plt.annotate('{}'.format(ang), xy=(inst[0], inst[1]), xytext=(-2, 2), ha='right',
                textcoords='offset points')
	for inst, ang in c[5:]:
		plt.plot(inst[0], inst[1], "ro")
		plt.annotate('{}'.format(ang), xy=(inst[0], inst[1]), xytext=(-2, 2), ha='right',
                textcoords='offset points')

	plt.grid(True)
	plt.show()

def main() :
	test()

if __name__ == "__main__":
    main()